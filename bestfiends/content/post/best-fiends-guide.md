+++
title = "Best Fiends Guide"
date = "2015-12-08"
categories = ["Guides"]
menu = ""
+++

Best Fiends is is a matching game that's great fun. While the art style is clearly targetted towards younger players, the game has great appeal to older gamers as well. This guide will teach you everything you need to know to beat Best Fiends. If you're looking for a quick read, checkout these [tips to best fiends](best-fiends-tips).

# Best Fiends Rules

The rules to Best Fiends start out simple and get more advanced as you play further into the game. New levels add new features to the game which make levels more challenging. In the beginning, these are the rules:

1. Each level has a limited number of moves.
2. Matches are removed from the board as they are matched.
3. New colors fall in from the top in a random order. Some levels start out with a set pattern.
4. A match must be at least 3 squares, but can be as big as the full board.
5. Matches damage and kill slugs.
6. Matches go towards your power ups. Power ups drop in from the top of the screen.
7. Every level is won by meeting goals will have goals such as match 45 greens, or get 3 diamonds.

# Basic Best Fiends Strategy

In general, go for the longest match on the board. There are some exceptions where other matches make sense. Matches that contribute to your goals are often more important than long matches. Focus on the goals. Sometimes, you can prepare longer matches by removing smaller matches that seperate a match. The best match in the game is a "Slugamegdon". Earning one of these is almost impossible without the bomb power up. Bomb power ups are great for making matches, but bomb power ups have the least power out of any fiend type. So, on slug kill missions, use bombs sparingly.

# Advanced Tactics for Best Fiends
Always complete a level even if you've lost because losing earns you additional resources, and losing levels sometimes gains you extra moves.  You also keep all power ups gained on a level that you lose. So, if you've lost, you can focus on collecting gems, keys and mites.

For slug kill missions, avoid bomb fiends. The bomb fiends do the least damage. Instead, choose your highest damage fiends. In fact, if you focus your upgrades on a single color fiend, you can get 1 fiend that's massively more powerful than the rest. This will give you a huge advantage. 

Lastly, make sure to save your gold for truly hard levels. Try a level 9 or 10 times before you consider using gold on it. The later stages of the game are very difficult and they require many plays to beat even if you play perfectly. This is because Best Fiends is random and sometimes, you just don't get the drops you need to win.
~Rylar
