+++
title = "Best Fiends Tips"
date = "2015-12-08"
categories = ["Guides"]
menu = ""
+++

The early levels of best fiends up to about 40 aren't too difficult. All you need to do is to make matches and you'll win levels. Progressing beyond the early stage of the game takes hardwork and skill. Here are some great Best Fiends Tips that will help you complete more levels quickly. Checkout my full guide for a more in-depth [guide to best fiends](best-fiends-guide).

# Give Me the Tips

* In general, make the longest match on the board.
* Focus on your goals. If you need a lot of 1 color, focus on matching that color often.
* Bombs convert squares to their color. So, if you need lots of a color, get that color's bomb.
* For slug kill levels, don't get more bombs than you need because bomb matches do less damage.
* Focus your upgrades on bomb fiends because those are the only fiends that will always need.
* For other upgrades, concentrate your upgrades to your highest damage fiend for each type.
* Don't waste upgrades on fiends you don't need.
* Once you get beyond level 50, expect to lose a few times. When you lose, don't quit! Finish the level for bonuses.
* When you lose enough times on a level, you get extra turns.
* If you really need to play more, there's only one [Best Fiends Cheat](best-fiends-cheat) that can help you.
* Spend diamonds on keys and not mites because keys get you more mites than buying them directly.
* Save gold for levels you are really, really stuck on.
* Do your best to beat the special colored levels because they can give you extra fiends!

~Rylar
