+++
title = "About"
date = "2015-10-02T21:49:20+02:00"
categories = ["Guides"]
menu = "main"
+++

Best Fiends is a [a puzzle matching game](https://bestfiends.com/best-fiends/) similar to Soda Crush. However, it has some unique features that make it fun and different. The best feature of Best Fiends is your ability to upgrade the power of your matches which gives you more powerful skills and more damage for your matches. Game Solver is a network of howto sites written by avid gamer [Greg Boggs](http://www.gregboggs.com).