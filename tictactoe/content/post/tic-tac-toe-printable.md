+++
title = "Tic Tac Toe Printable"
date = "2015-12-02"
categories = ["Guides"]
menu = ""
+++

So, you've ready my [Tic Tac Toe strategies](tic-tac-toe-strategy), and you're ready to win. So, print out some of these boards, and it's game on!

#Drawing Your Own Boards

To draw your own Tic Tac Toe board, just make two parallel lines. Now make two more parallel lines perpendicular to the first two lines.