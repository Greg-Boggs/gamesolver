+++
title = "How to Play Tic Tac Toe"
date = "2015-12-08"
categories = ["Guides"]
menu = ""
+++

Tic Tac Toe is a fun game fit for any age. It can be played with a piece of paper and pencil. For extra fun, used crayons. Or, add shapes and doodles to your game boards.

#Rules

1. The game is played on a 3 by 3 board of 9 squares.
2. X makes the first move.
3. Each player takes turns covering squares.
4. The first player to connect 3 in a row wins.

#Introduction
The X's have an advantage. So, when you are O's, just try and block your opponent. You'll need to lose a lot of games of Tic Tac Toe to get the basics down.

#Block Those X's
The most basic move to learn is the block. The first thing to look for before moving is 2 in a row. If your opponent has 2 in a row, immediately block them. The block is always a good move. You might still lose if you block, but most of the time, you'll earn a Cat's game or draw game.

#How to Win Tic Tac Toe
If you really want to win, the best way is getting 2 in a row in 2 directions with 1 move. This sounds hard, but you can learn [how to win Tic Tac Toe](how-to-win-tic-tac-toe) by mastering a few strategies.

If you're not sure where to go, go in the center or a corner! Corners are usually better than the edges. Once you've got the basics, the advanced [Tic Tac Toe strategies](tic-tac-toe-strategies) will help you beat even the toughest players.

