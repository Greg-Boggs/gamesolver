+++
title = "Tic Tac Toe Rules"
date = "2015-12-03"
categories = ["Guides"]
menu = ""
+++

Tic Tac Toe is classic game of strategy. The rules aren't hard to learn, but mastering the game is difficult.

#Rules
 The rules of Tic Tac Toe play are:

1. The game is played on a 3 by 3 board of 9 squares.
2. X makes the first move.
3. Each player takes turns covering squares.
4. The first player to connect 3 in a row wins.

#Strategy
The player who goes first has the advantage. So, it's best to let the least experienced player go first. After the first game, the players can take turns going first. Many games of Tic Tac Toe end in a tie. A Tie is called a "Cats Game". But, there are many strategies to help you win. 

#Tips for Playing with Kids
* Show them an example of how to win.
* Help them learn to block by letting them know when you get 2 in a row. 
* Try to get several cat's games in a row. 
* Let the youngest child go first.