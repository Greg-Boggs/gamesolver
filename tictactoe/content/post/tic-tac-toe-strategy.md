+++
title = "Tic Tac Toe Strategy"
date = "2015-12-04"
categories = ["Guides"]
menu = ""
+++

Mastering Tic Tac Toe can be a frustrating. It takes practice, and these Tic Tac Toe strategies will help you do it. 

#Basic Strategies
The X's have an advantage. So, be X first. When it's your turn to play O's, don't try to win. Just block and get a Cat's game. Your chance to win will come when you are X's.

Don't play the same game twice. Instead, learn 2 or 3 strategies for winning and switch between them on each game. Any move in Tic Tac Toe can be rotated. So, make sure to rotate your moves from game to game. The more your oponnent has to think, the better chance you have to win.

If you lose a game, immediately ask for "best 2 out of 3". This classic tactic will get you another chance to win when you've already lost. If you ask just as your oponnent wins, their excitement at winning should lead them to play you again. 

#Advanced Strategies
The only way to reliably win at Tic Tac Toe is to give yourself 2 ways to connect 3 in a row. This is often called a double. Once you have a double, you've won the game. Here are 3 methods for creating doubles. Switch between these and rotate them on the board as you play.

1. Go in the center first. If O's doesn't go in the corner, they've lost!
2. Go in the corner first. 
3. Split the center in the corners.

#Practice, Practice, Practice
Go play a computer a few hundred times as fast as you can. You won't win. Ever. Yes. You will never win. Because Tic Tac Toe is [solved by computers](http://perfecttictactoe.herokuapp.com/). You've seen War Games right? But, you'll learn the game quickly by playing a lot of games fast.