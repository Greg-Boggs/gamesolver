+++
title = "How to Win Tic Tac Toe"
date = "2015-12-08"
categories = ["Guides"]
menu = ""
+++

Mastering Tic Tac Toe is difficult. When playing an experienced player, winning a game of Tic Tac Toe is extremely challenging. But, here are some basics that will help you win.

#Rules

1. The game is played on a 3 by 3 board of 9 squares.
2. X makes the first move.
3. Each player takes turns covering squares.
4. The first player to connect 3 in a row wins.

#Basics
The X's have an advantage. So, be X first. Don't be predictable. Changing your game makes it harder for the other player. When playing as O's your goal is to tie the game. Don't try to win. Just block and end the game so that you can be X's.

#How to Win Tic Tac Toe
The way to win is to get 2 in a row in 2 directions with 1 move. Once you get this double, you've won the game no matter where the other player goes because they cannot block 2 directions in 1 move. It sounds hard, but there are many patterns that will give you a double. 

One way to win is to go in the center. If your opponent doesn't go in a corner, they've lost. Don't use this trick too many times or they might catch on too quickly!

When your opponent misses the corner, immediately go in the corner. It doesn't matter which one. Now, they are forced to block you. Next, you may have to block them. But, if not, go in the space that gives you 2 in a row in 2 directions! Once you have this double move mastered read more advanced Tic Toe Strategy.