+++
title = "About"
date = "2015-10-02T21:49:20+02:00"
categories = ["Guides"]
menu = "main"
+++

Game Solver is a network of howto sites written by avid gamer [Greg Boggs](http://www.gregboggs.com). This Tic Tac Toe guide started from a popular YouTube video on how to [never lose at Tic Tac Toe](https://www.youtube.com/watch?v=wxJPzcBzZxo). The video is great and has a couple of strategies. But, it was clearly missing a common method for winning. So, Game Solver's [Tic Tac Toe Guide](tic-tac-toe-strategy)is here.