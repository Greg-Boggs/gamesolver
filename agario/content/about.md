+++
title = "About"
date = "2015-10-02"
categories = ["Guides"]
menu = "main"
+++

This is the best, most comprehensive guide to Agario. Agario is a free to play, massive, multiplayer game where players constantly battle to absorb each other. The guide covers beginners how-to-play basics, tips, and advanced strategies for top play. Game Solver is a network of game guides written by avid gamer [Greg Boggs](http://www.gregboggs.com).

My favoriate [Agario strategy](agario-strategy) is to base build in a corner. You'll find me playing the app version of Agario as Rylar with no skins. 