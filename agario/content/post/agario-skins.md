+++
title = "Agario Skins"
date = "2015-12-27"
categories = ["Guides"]
menu = ""
+++

Agario comes with many great skins built right into the game that are free to use. Some of the most popular skins include: Earth, Mars, Nasa, and even memes like Doge! For a full list of skins, check the [Agario Skin List](http://pastebin.com/kXQDhQEr). 

#How to use an Agario Skin
To use an Agario skin, just change your name to the skin you want to use. That's it. That means you cannot use a skin with a name of your own choosing. However, in the app version of Agario, there are many skins available for purchase using both real money and in game coins that you can earn by playing. 

#Custom Skins
A lot of people want a custom skin. Now with the mobile app, you can earn the ability to use one!. It's one of the hardest achievements, but it's totally worth it.

So, if you love Agario skins, get the mobile app and get playing. You'll eventually unlock some great skins. However, if you've read my [Agario Tips](agario-tips), you'll know not to use the spider or hornet skins earned by new players because they give away your lack of experience. However, if you're a real professional gamer, you might choose the spider skin to throw off other players!

~Rylar