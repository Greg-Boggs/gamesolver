+++
title = "Agario Strategy"
date = "2015-12-27"
categories = ["Guides"]
menu = ""
+++

This guide will teach you advanced Agario strategy to improve your game. You should read the [beginner how to](how-to-play-agario), and the full [Agario Guide](agario-guide) because this guide only covers advanced tactics and strategies.

# Team Up
Some would consider this advanced strategy to be an [Agario Cheat](agario-cheats-hacks), but playing a team well is a real challenge. You have a big advantage in a team because you can see farther, split without fear, and hold more mass between two players than one player can hold alone. But, teams can still be beaten. For anti-team tactics, check out these amazing Jumbo [team](https://www.youtube.com/watch?v=b6vtbprgOWc) [busting](https://www.youtube.com/watch?v=Xj-IxOtlc2c) videos.

# Build a Wall
During the early game, you can die without much loss. So, use your freedom to split. This expands your vision greatly. Keep your two blobs far apart and scout the map a bit. Look for the edges and corners. Once you find a corner or edge, follow along it and find some virus... Ideally, you want the virus far enough from the wall that you can fit between the virus and the wall, but the top players won't be able to do the same.

Next, focus on growing. As you grow, use your mass to expand the viruses virtically up and down the wall in a line. Don't worry about dying. If you die, just go right back to your wall and keep working on it. Make sure to keep enough space behind your wall for you to become large. Once your wall is strong, grow, grow, grow. Your wall will protect you from top players and allow you to get to the leaderboard. Once you're in the top 10, punch a hole in the wall and sneak out. Carefully scout your surroundings looking for other large blobs. If one chases you, run back behind your wall. Then, use your wall as a weapon to shoot viruses!

# Capture a Corner
Corners are ideal for capturing smaller players. Even though they are much faster, you can corner them easily. You can use viruses to protect the corner much more easily. Just make sure you don't trap yourself in the corner. One key strategy for corner play is to leave enough space between yourself and the corner that smaller players mistakenly enter the corner trap only to be absorbed.

# Practice Shooting Viruses
Even a mid sized blob can do serious damage to top players using viruses. Get used to firing viruses at other players so when you do become a big player, you're ready for the final stages of the game.

# Absorb Viruses
That's right, when you are large enough, you can absorb viruses without taking any damage. The trick is to split a few times. Once you have a small blob or two, run the smallest blobs into a virus. It will split into many tiny bubbles and then you won't be able to split anymore which means you can now freely eat viruses without being split!