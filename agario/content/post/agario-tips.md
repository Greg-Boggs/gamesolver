+++
title = "Agario Tips"
date = "2015-12-27"
categories = ["Guides"]
menu = ""
+++

Have you read my beginners guide in [how to play Agario](how-to-play-agario)? Go ahead, read it. I'll wait. There are several Agario tips that will help you climb the leaderboard. The best tip is to don't be afraid to die. You aren't trying to win yet. You're just trying to learn the ropes. So, play a lot of games quickly. To that end, split often. If you get lucky, you grow. Practice splitting as often as you can. In fact, Jumbo, [one of the best Agario players](https://www.youtube.com/channel/UC_17vC75UDfHgTJjX5Ravgw) splits into 4 blobs as early as he can. Beyond practicing, there are a few quick tips that will really help your game.

# Agario Tips

* When you're super small, keep moving, but don't bounce around. Pick your course, and stay in a steady direction, but still make changes. Big players can see farther than you and they will follow you hoping that you will turn around. Stay the course.
* If you are smaller, be careful chasing a group of blobs. Split up blobs are attractive bait. Don't get caught up in the crossfire. On the otherhand, if you're growing and bigger than most your neighbors, split as soon as you see a group of blobs.
* Try to get an understanding of where you are on the board because orners can be dangerous for small blobs and you need to know where bigger players are so you can avoid them as you grow.
* When you're a bit bigger, the virus blobs are your friend. Bigger blobs can't eat you when you hide behind virus.
Another great tip for Agario is to find a super blob and coast around eating the dots the big blob misses. Super blobs won't care for such a small snack, but they will keep the bigger aggressive blobs safely away. Just be careful not to 
* grow too large near a massive agario player, otherwise, you'll be lunch!
* When you get a bit bigger, use the virus to make a wall. Create a safe space for yourself that big agario players can't get to.
* A great tip for Agar.io once you start growing is to keep an eye on the leader board. It will tell you if you're larger than blobs near your own size.
* When playing Agario teams, stick near large blobs of your own color. But, if they get in trouble, run!
* In the Agario mobile app, avoid using the achievement skins used by newbies. It will give away your lack of experience.

# Agario Tricks

* If you're having trouble, one trick is to avoid the new player skins. They let other players know that you're new. 
* Finally, if you're really struggling, consider playing with a friend. The two of you can feed each other when you split.

# Perfect Agario Tip
If you're the same size as the virus, shoot 2-4 small blobs into the virus to grow it. Then eat a few more dots until you're the same size as the virus again. Now, line yourself up under the virus and wait. You won't be seen and eventually, someone just smaller than you will try to duck through the virus and you'll nearly double in size.

Want to know more? Checkout my advanced [agario strategy](agario-strategy) guide.
Happy Hunting!
~Rylar