+++
title = "Agario Guide"
date = "2015-12-28"
categories = ["Guides"]
menu = ""
+++

This Agario guide will take you step by step through the process of starting at the bottom and ending up on top of the leaderboard. 

#Early Stages
The early stages of Agario is chaotic. As a small blob, vision is limited. This makes it extremely easy to wonder into larger players. The high speed of the smallest blobs makes it even harder not wonder into other players. The trick to getting through this stage is to be efficient in collecting passive dots and to stay on the look out for smaller players. Be careful not to wonder backwards because your small size will attract larger players who can easily follow you hoping that you will turn around and crash into them. Finally, be careful of viruses because larger players can hide under them to trick you.

* Advanced Tip: split as soon as you can especially if you see a small player to eat. This will increase your vision dramatically and increase your total area and speed. This move will attract more players chasing you, but the risk can be worth it.

#Beyond the early stages
Once you're large enough to start eating other smaller dots, it becomes safe to split. But, try not to split twice and be extra cautious while divided. At this stage, use the virus to protect yourself. Mid-sized blobs further along in the stages of this guide to Agario, won't be able to split easily around virus. However, hiding amoung the viruses is a popular tactic. So you might find the virus cloud popular with hunters. This is one of the hardest stages of the game. Keep your finger hovering over the split action and be ready grow fast.

#Midstage hunting
If you've survived this long, you'll quickly find that you're larger than everyone around you. Your new problem is that you're now so slow, that even this Agario guide can't help you catch other players. Focus on splitting to capture smaller players. Do your best to judge players just smaller than half your size. And, be on the look out for large players split too many times. 

This is the first time where you can really own the virus fields. You're still too fast to be an easy target for viruses. So, you can use them to trap other players. Fianally, consider moving towards the corner or edge where you can use edges and corners to trap players. But, be on the look out for players in the final stages of Agario. They will split often more than once at the first site of a tasty snack.

* Advanced Tip: If you run into another player of your own size, don't get too close. Instead, use them as a pincer to trap smaller, faster players between you. 

#Leaderboard
Congradulations! You've hit the leaderboard. Now what? Open it! The leaderboard will tell you who you can eat and who can eat you. If you see someone roughly small enough to eat, check the leaderboard, they need to be 2-3 slots below you to be eaten. At this stage, you feel very powerful. But super, super slow. The only way to eat other players now is to split. So, split, split, split. But, be very careful of larger players. At rank 8 or 9, you're in a very weak spot. If a larger player sees you, they will follow you across the map for a chance to double in size. So, virus are your only protection. Keep tabs on virus that you can just barely squeeze through. But, be careful because virus at this stage become truely dangerous.

* Advanced Tip: You can create a virus wall to protect yourself from top players. Just split viruses in a line to block off an area for yourself. This works best in the corners. But, be careful to give yourself enough room to grow. 

#Number 1
The virus is now your number 1 enemy. Stay far away! Usually, you get to this stage by coming across the #1 blob as it has been split by a virus, or perhaps the player has split too many times being greedy. The #1 blob is constantly decreasing in size, so it must eat constantly just to stay the same size. Now that you've absorbed the largest player, this is your problem now. You can now split easily and gobble up large groups of players. But, you need to be constantly aware of where the other top players are. If you are split, they can eat you, and they will fire viruses at you. The only way to stay on top is to constantly absorb players through splitting. So, split away. But, be warned, there's always someone just behind you looking to be the biggest.

* Advanced tip: If you've been hit by a virus, you can still absorb the player attacking you by splitting! Also, when you've split into enough pieces, you can safely use your smaller pieces to remove virus from the board. Running them down will clear out space for yourself without causing you to split more.

~Rylar

