+++
title = "How to Play Agario"
date = "2015-12-27"
categories = ["Guides"]
menu = ""
+++

Agario is a fun, but brutal game. The game can be stressful for young children, but it's very fun for those who can handle losing often and trying again. The concept of the game is simple. Eat or be eaten. Suck up as many dots as you can before  someone bigger comes along and takes you out. I hope to teach you the basics of how to play Agario with this game guide.

#Rules
1. When two players touch, the larger player eats the smaller one.
2. Smaller blobs are faster than larger blobs.
3. A player can split increasing it's speed, and launching an attack with the second blob.
4. A split player is more vulnerable. 
5. Larger players split into smaller pieces when they touch a green virus.
6. A player can fire small pieces into a virus using them as weapons against other players.

#Advice for Beginners Playing Agario
Don't worry about dying. You will die a lot in Agar.io. Just roll with it and restart. Play fast. Take risks. Lose a lot. After you've played for a few days. Take a break from Agario and do something else! The break will do you go. During your break consider watching some top notch [Jumbo Videos](https://www.youtube.com/channel/UC_17vC75UDfHgTJjX5Ravgw).

#Agario Tips
* If you're having trouble surviving, stay near the viruses, but be careful not to run into them. 
* If you manage to grow bigger, Try to find a wall of viruses to hide behind. 
* Try splitting more often!

#How to Win Agario
You can't win Agario! Once you become the largest player, your blob starts slowly shrinking. Even if you eat the second largest blob over and over, you still can't grow any more. Eventually, you'll be hit with a virus, or caught while split.
Even the best players can't stay on top for ever.

Ready to learn more? Check out my [Agario Guide](agario-guide) next.
~Rylar