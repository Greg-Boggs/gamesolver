+++
title = "Minecraft Water"
date = "2015-12-08"
categories = ["Blocks"]
menu = ""
+++

Water is one of the most interesting blocks in Minecraft because it's one of just 2 blocks that can flow. Water in Minecraft doesn't flow like water in real life. Instead, water flows down blocks up to a distance of 5 blocks. The way water flows is one of the most confusing aspects of water for new players.

# Special properties

* Water can produce obsidian, cobblestone, and stone when dumped on lava. In fact, water is a key ingredient to a [cobblestone generator](builds/coblestone-generator).
* To move water, you must pick it up with an [empty bucket](items/empty-bucket).
* Water prevents mobs from spawning. 
* Water is semi-transparent which means you can see through it, but deep water gets very dark.
* Water can put out fires and prevent them from spreading.
* Water can be used to collect items. So, it's often used in [automatic farms](builds/automatic-farms).

# Water Builds

There are several great builds you can do with water. Water slides are popular, but you can also make lakes, and [droppers](builds/minecraft-droppers).