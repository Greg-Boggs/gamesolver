+++
title = "How to Make a Minecraft Lake"
date = "2015-12-08"
categories = ["Builds"]
menu = ""
+++

Making a large body of perfectly flat water is a challenge. Instead of trying to dump water over a large deep hole which will fail, dig a 1 block deep hole. Cover the entire hole with water. Once the 1 deep hole has been filled. Break out the blocks under the water. Tada, you now have a perfectly flat lake. There are [TNT mods](mods/tnt-mods) that can spawn oceans more quickly.

# Needed Items
To make a lake, you'll need an [infinite water](builds/minecraft-infinite-water) source, a water bucket, and a shovel is also nice.